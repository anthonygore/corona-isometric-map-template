--------------------------------------------------------------------------------
-- VARIABLES
--------------------------------------------------------------------------------

local storyboard = require( "storyboard" )
local scene = storyboard.newScene()

--Map
local gridmap 
local map
local mapData

--Groups
local blockGroup
local UIgroup

--Constants
local mapWidth
local mapHeight
local tileWidth
local tileHeigh

--UI
local widget
local DpadL
local DpadR
local txt

--Blocks
local block
local selected
local coverTile

--Sounds
local blockClick
local winSound

--Functions
local onEveryFrame
local mapOffset
local moveMap
local angleToDir
local moveBlock
local newBlockPos
local createDPad
local reorderDisplay
local colourBlocks
local addEventListeners
local removeEventListeners
local resetOffset


---------------------------------------------------------------------------------
-- STORYBOARD
---------------------------------------------------------------------------------

function scene:createScene( event )
    widget = require( "widget" )
    gridmap = require("gridmap")
    mapData = require "map.map2"
    mapWidth = mapData.width - 4 --Subtract the borders
    mapHeight = mapData.height - 4 --Subtract the borders
    tileWidth = mapData.tilewidth
    tileHeight = mapData.tileheight      
        
    --Display groups
    local group = self.view
    blockGroup = display.newGroup()

    --Variables
    block = {}
    selected = 0 --i.e. which block is selected, default is 0
    blockClick = audio.loadSound( "sounds/54405__korgms2000b__button-click.wav" )
    winSound = audio.loadSound( "sounds/162473__kastenfrosch__successful.mp3" )
    coverTile = {x = 4, y = 2} --i.e. which tile needs to be covered to win the game

    --Functions
    function angleToDir (angle)
        local x, y = 0, 0
        if (angle >= 315 and angle <= 360) or (angle >= 0 and angle < 45) then
            --North
            x = 0
            y = -1
        elseif angle >= 45 and angle < 135 then
            --East
            x = 1
            y = 0
        elseif angle >= 135 and angle < 225 then
            --South
            x = 0
            y = 1
        else
            --West
            x = -1
            y = 0
        end
        return x,y
    end

    --A wrapper for mapOffset when using the Dpad to move it
    function moveMap(angle)
        local x, y = angleToDir(angle)
        mapOffset(x, y)
        
        --Remove the map and redraw
        map:removeSelf()
        map = gridmap:createMap(mapData)
        group:insert(1, map)
    end
    
    --Moves the view of the map by adding an offset
    function mapOffset(x, y)
        --Move each layer
        for i = 1, #mapData.layers do
            if mapData.layers[i].type == "tilelayer" then
                mapData.layers[i].x = mapData.layers[i].x - x 
                mapData.layers[i].y = mapData.layers[i].y - y
            end
        end
        
        --Move all the blocks as well but keep their relative position
        local moveX = (x - y) * tileWidth/2;
        local moveY = (x + y) * tileHeight/2;
        for i = 1, #block do
            block[i].x = block[i].x - moveX
            block[i].y = block[i].y - moveY
        end
    end
    
    --Used to ensure the blocks are displayed in the correct order. Turn it off
    --and you'll see why it's important...
    function reorderDisplay()
        
        --Put the blocks into an ordered table
        local ordered = {}
        for i = 1, #block do
            table.insert(ordered, block[i])
        end
        
        --Arranges the blocks based on the smallest y value 
        table.sort(ordered, 
        function(a, b)
            return a.y < b.y
        end)
        
        --Inserts the blocks into the display group in that order i.e. the
        --smallest goes in first, the largest goes in last.
        for i = 1, #ordered do
            ordered[i]:toFront()
        end
    end
    
    --The logic behind sliding the blocks
    function newBlockPos(iX, iY, jArr, dX, dY)
        --Stores the points i could move to, given each j
        local p = {}
        local i = {x=iX, y=iY}
        local d = {x=dX, y=dY}
        
        --Iterate each j
        for n = 1, #jArr do 
            p[n] = {}
            --Simplify the inputs a bit            
            local j = {x=jArr[n].tileX, y=jArr[n].tileY}

            -- X-axis
            if d.x > 0 then
                --if moving right
                if (j.y - i.y) == 0 and j.x > i.x then
                    --if the other block is in the way
                    p[n].x = j.x - 2
                else
                    --nothing in the way
                    p[n].x = mapWidth - 2
                end
            elseif d.x < 0 then
                --if moving left
                if (j.y - i.y) == 0 and i.x > j.x then
                    --if the other block is in the way         
                    p[n].x = j.x + 2
                else
                    --nothing in the way
                    p[n].x = 0
                end
            else
                --only moving on y-axis
                p[n].x = i.x
            end

            -- Y-axis
            if d.y > 0 then
                --if moving down
                if (j.x - i.x) == 0 and j.y > i.y then
                    --if the other block is in the way
                    p[n].y = j.y - 2
                else
                    --nothing in the way
                    p[n].y = mapHeight - 2
                end
            elseif d.y < 0 then
                --if moving up
                if (j.x - i.x) == 0 and i.y > j.y then
                    --if the other block is in the way         
                    p[n].y = j.y + 2
                else
                    --nothing in the way
                    p[n].y = 0
                end
            else
                --only moving on y-axis
                p[n].y = i.y
            end
        end
        
        --Block i can move to the closest of the points recorded
        if d.x > 0 then
             --if moving right
             --Order table by .x
            table.sort(p, function(a,b) return a.x < b.x end)
            for n = 1, #p do
                if p[n].x >= i.x then
                    return p[n].x, p[n].y
                end
            end
        elseif d.x < 0 then
            --if moving left
            --Order table by .x
            table.sort(p, function(a,b) return a.x > b.x end)
            for n = 1, #p do
                if p[n].x <= i.x then
                    return p[n].x, p[n].y
                end
            end
        elseif d.y > 0 then

             --if moving down
            --Order table by .y, smallest to largest
            table.sort(p, function(a,b) return a.y < b.y end)
            for n = 1, #p do
                if p[n].y >= i.y then
                    return p[n].x, p[n].y
                end
            end
        else
            --if moving up
            --Order table by .y, largest to smallest
            table.sort(p, function(a,b) return a.y > b.y end)
            for n = 1, #p do
                if p[n].y <= i.y then
                    return p[n].x, p[n].y
                end
            end
        end
    end
    
    --Colours a block
    function colourBlocks(i)
        for n = 1, #block do
            if n == i then
                block[n]:setFillColor( 0.8, 0.3, 0.3 )
            else
                block[n]:setFillColor( 1, 1, 1 )
            end       
        end
    end
    
    --Called when the cover tile is covered by a block
    function gameWon ()
        audio.play(winSound)
        local winTxt = display.newEmbossedText( "You Win!", display.contentWidth/2, display.contentHeight/2, native.systemFont, 48 )
        winTxt:setFillColor( 0.1, 0.2, 1)
        local color = 
        {
            highlight = { r=0, g=0, b=1},
            shadow = { r=0, g=0, b=0 }
        }
        winTxt:setEmbossColor( color )
        group:insert(winTxt)
            
        -- Function to handle button events
        local function handleButtonEvent( event )
            if ( "ended" == event.phase ) then
                winTxt:removeSelf()
                event.target:removeSelf()
                storyboard.gotoScene("reload")
            end
        end

        -- Create the widget
        local button1 = widget.newButton
        {
            x = display.contentWidth/2,
            y = (display.contentHeight/2) + 55,
            label = "Press to restart",
            fontSize = 24,
            labelColor = { default={ 0, 0, 0 }, over={ 0, 0, 0, 0.5 } },
            onEvent = handleButtonEvent
        }
        group:insert(button1)

    end
    
    --A function to check if the player has won i.e. covered the tile
    function checkWon()
        for i = 1, #block do
            if block[i].tileX == coverTile.x and block[i].tileY == coverTile.y then
                gameWon()
            end
        end
    end

    --Moves a block
    function moveBlock(angle, i)
        if i > 0 then
            --Variables
            local deltaX, deltaY, newTilePosX, newTilePosY, iTileX, iTileY, 
            moveScreenPosX, moveScreenPosY, newScreenPosX, newScreenPosY, 
            deltaTilePosX, deltaTilePosY, timeScale

            --Arrays
            local jArr = {}

            --Only move if the block is not already moving
            if not block[i].moving then

                --Get directional change
                deltaX, deltaY = angleToDir(angle)

                --Get position of current block
                iTileX, iTileY = block[i].tileX, block[i].tileY

                --Get position of the other blocks
                for j = 1, #block do
                    if j ~= i then
                        table.insert(jArr, { tileX = block[j].tileX, tileY = block[j].tileY})
                    end
                end

                --Calculate new block position         
                newTilePosX, newTilePosY = newBlockPos(iTileX, iTileY, jArr, deltaX, deltaY)

                --Change in block position
                deltaTilePosX, deltaTilePosY = newTilePosX - iTileX, newTilePosY - iTileY 

                --See if new position is viable
                if (newTilePosX >= 0 and newTilePosX < (mapWidth - 1)) and (newTilePosY >= 0 and newTilePosY < (mapHeight - 1)) then

                    --Update tile position
                    block[i].tileX, block[i].tileY = newTilePosX, newTilePosY

                    --Calculate new screen position
                    moveScreenPosX, moveScreenPosY = (deltaTilePosX - deltaTilePosY) * tileWidth/2, (deltaTilePosX + deltaTilePosY) * tileHeight/2
                    newScreenPosX, newScreenPosY = block[i].x + moveScreenPosX, block[i].y + moveScreenPosY

                    --Set the block to moving
                    block[i].moving = true

                    --Update screen position with a transition
                    local function listener (event)
                        block[i].moving = false
                        if moveScreenPosX == 0 and moveScreenPosY == 0 then
                            
                        else
                            audio.play(blockClick)
                        end
                        --Check if the game's been won
                        checkWon()
                    end

                    if math.abs(deltaX) > math.abs(deltaY) then
                        timeScale = math.abs(deltaTilePosX)
                    else
                        timeScale = math.abs(deltaTilePosY)
                    end

                    transition.to(block[i], {time=timeScale*100, transition=easing.outQuad, x = newScreenPosX, y = newScreenPosY, onComplete = listener})
                end
            end
        end
    end    
    
    --Map
    --Change offset straight away
    mapOffset(-5,5)
    map = gridmap:createMap(mapData)
    group:insert(1, map)
        
    function createDPad (x,y)
        local Dpad = display.newImageRect("images/dpad.png", 100, 100)
        Dpad.x = x
        Dpad.y = y
        Dpad.holding = false
        Dpad.angle = nil
        Dpad:toFront()
        return Dpad
    end
    
    --Blocks
    for i = 1, 3 do
        local options = {
            width = 80,
            height = 80,
            numFrames = 1,
        }
        local sheet1 = graphics.newImageSheet( "images/1406200302_sealed-wood-box.png", options )
        local sequenceData = {
            name = "basic",
            start = 1,
            count = 1,        
            loopCount = 0,    
            loopDirection = "forward"
        }
        block[i] = display.newSprite( sheet1, sequenceData )
        block[i].name = i
        block[i].moving = false
        blockGroup:insert(block[i])
        local start = map.layers[1].tiles[12 + (i*2)]
        block[i].tileX = 0 + ((i-1)*2)
        block[i].tileY = 0
        block[i].anchorX = 0.55
        block[i].anchorY = 0.17
        block[i].x = start.x
        block[i].y = start.y
    end
    
    group:insert(blockGroup)

    --Dpads
    UIgroup = display.newGroup()
    
    --DpadR
    DpadR = createDPad(display.contentWidth - 50, display.contentHeight -50)
    DpadR.direction = nil

    DpadR.getRotationAngle = function (begX, begY, endX, endY)
        local dirX = begX - endX
        local dirY = begY - endY
        local angle = math.deg(math.atan(dirY/dirX))
        if dirX < 0 then
            angle = 90 + (90 - (angle * -1))
        end
        angle = angle + 90
        if angle == 360 then
            angle = 0
        end
        return angle
    end
    UIgroup:insert(DpadR)
    
    --DpadL
    DpadL = createDPad(50, display.contentHeight -50)
    DpadL.direction = nil

    DpadL.getRotationAngle = function (begX, begY, endX, endY)
        local dirX = begX - endX
        local dirY = begY - endY
        local angle = math.deg(math.atan(dirY/dirX))
        if dirX < 0 then
            angle = 90 + (90 - (angle * -1))
        end
        angle = angle + 90
        if angle == 360 then
            angle = 0
        end
        return angle
    end
    
    UIgroup:insert(DpadL)
    group:insert(UIgroup)
    
    --Add event listeners
    function addEventListeners()
        --DpadL
        function DpadL:touch (event)
            if event.phase == "began" then
                display.getCurrentStage():setFocus(event.target, event.id) 
                event.target.isFocus = true
            end
            if event.phase == "ended" or event.phase == "cancelled" then
                moveBlock(self.getRotationAngle(event.x, event.y, event.target.x, event.target.y), selected)
                display.getCurrentStage():setFocus( event.target, nil )
                event.target.isFocus = false
            end
            return true
        end
        DpadL:addEventListener("touch", DpadL)
        
        --DpadR
        function DpadR:touch (event)
            if event.phase == "began" then
                display.getCurrentStage():setFocus(event.target, event.id) 
                event.target.isFocus = true
            end
            if event.phase == "ended" or event.phase == "cancelled" then
                moveMap(self.getRotationAngle(event.x, event.y, event.target.x, event.target.y))
                display.getCurrentStage():setFocus( event.target, nil )
                event.target.isFocus = false
            end
            return true
        end
        DpadR:addEventListener("touch", DpadR)
        
        for i = 1, #block do
            --Blocks
            local function listener (event)
                if event.phase == "began" then
                    selected = i
                    colourBlocks(selected)
                end
            end
            block[i]:addEventListener("touch", listener )
        end
    end
end

function scene:enterScene( event )
    local group = self.view
    --Start game
    local options = 
    {
        text = "Move the boxes to cover the water square",
        x = display.contentWidth/2,
        y = display.contentHeight/2+25,
        width = display.contentWidth*2/3,
        height = 100,
        font = native.systemFont,
        fontSize = 28,
        align="center"
    }
    txt = display.newEmbossedText(options)
    txt:setFillColor( 0.1,0.2,1)
    local color = 
    {
        highlight = { r=0, g=0, b=1 },
        shadow = { r=0, g=0, b=0 }
    }
    txt:setEmbossColor( color )
    group:insert(txt)

    local function txtListener1 (event)
        if event.phase == "ended" then
                txt:removeEventListener("touch", txtListener1 )
                txt:removeSelf()
                addEventListeners()
        end
    end
    txt:addEventListener("touch", txtListener1 )
    
    --Game loop
    function onEveryFrame (event)     
        --Check if it needs to reorder the block display
        for i = 1, #block do
            if block[i].moving == true then
                reorderDisplay()
            end
        end
    end
    
    Runtime:addEventListener( "enterFrame", onEveryFrame )
end


function scene:exitScene( event )
    local group = self.view
    function resetOffset()
        --Move each layer
        local xPos, yPos
        for i = 1, #mapData.layers do
            if mapData.layers[i].type == "tilelayer" then
                mapData.layers[i].x = 0
                mapData.layers[i].y = 0
            end
        end
    end
    Runtime:removeEventListener( "enterFrame", onEveryFrame )
    map:removeSelf()
    blockGroup:removeSelf()
    UIgroup:removeSelf()
    group:removeSelf()
    storyboard.purgeScene("game")
    resetOffset()
end

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched before next scene's transition begins
scene:addEventListener( "exitScene", scene )

return scene



