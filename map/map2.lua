return {
  version = "1.1",
  luaversion = "5.1",
  orientation = "isometric",
  width = 12,
  height = 12,
  tilewidth = 60,
  tileheight = 30,
  properties = {},
  tilesets = {
    {
      name = "isometric_new_tiles_by_spasquini-d4jnp2i",
      firstgid = 1,
      tilewidth = 60,
      tileheight = 51,
      spacing = 0,
      margin = 0,
      image = "images/isometric_new_tiles_by_spasquini-d4jnp2i.png",
      imagewidth = 960,
      imageheight = 51,
      properties = {},
      tiles = {}
    },
  },
  layers = {
    {
      type = "tilelayer",
      name = "Tile Layer 1",
      x = 0,
      y = 0,
      width = 12,
      height = 12,
      visible = true,
      opacity = 1,
      properties = {},
      encoding = "lua",
      data = {
        6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
        6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
        6, 6, 5, 5, 5, 5, 5, 5, 5, 5, 6, 6,
        6, 6, 5, 5, 5, 5, 5, 5, 5, 5, 6, 6,
        6, 6, 5, 5, 5, 5, 4, 4, 5, 5, 6, 6,
        6, 6, 5, 5, 5, 5, 4, 4, 5, 5, 6, 6,
        6, 6, 5, 5, 5, 5, 5, 5, 5, 5, 6, 6,
        6, 6, 5, 5, 5, 5, 5, 5, 5, 5, 6, 6,
        6, 6, 5, 5, 5, 5, 5, 5, 5, 5, 6, 6,
        6, 6, 5, 5, 5, 5, 5, 5, 5, 5, 6, 6,
        6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
        6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6
      }
    }
  }
}
